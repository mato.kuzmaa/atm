package sk.martin.ATM;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

public class Atm extends JFrame {

    private static final int INTRODUCTION = 0;
    private static final int DEPOSIT = 1;
    private static final int WITHDRAWAL = 2;


    private JPanel contentPane;
    private JTextArea tfDisplay;

    private JButton buttonDeposit, buttonWithdrawal, buttonBalance, buttonStatement;
    private JButton buttonUser, buttonAccount, buttonEnter, buttonDelete, buttonEnd;

    private JButton button1, button2, button3, button4, button5;
    private JButton button6, button7, button8, button9, button0;

    private String price = "";
    private int condition = INTRODUCTION;

    private final Account account = new Account("Martin", "SK9511000000001234567891", 0, "0914777777");

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    Atm frame = new Atm();
                    frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Create the frame.
     */
    public Atm() {

        initComponents();
        createEvents();

    }

    private void createEvents() {

        button1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (condition == DEPOSIT || condition == WITHDRAWAL) {
                    price += "1";
                    tfDisplay.setText(price);
                }
            }
        });

        button2.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (condition == DEPOSIT || condition == WITHDRAWAL) {
                    price += "2";
                    tfDisplay.setText(price);
                }
            }
        });

        button3.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (condition == DEPOSIT || condition == WITHDRAWAL) {
                    price += "3";
                    tfDisplay.setText(price);
                }
            }
        });

        button4.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (condition == DEPOSIT || condition == WITHDRAWAL) {
                    price += "4";
                    tfDisplay.setText(price);
                }
            }
        });

        button5.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (condition == DEPOSIT || condition == WITHDRAWAL) {
                    price += "5";
                    tfDisplay.setText(price);
                }
            }
        });

        button6.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (condition == DEPOSIT || condition == WITHDRAWAL) {
                    price += "6";
                    tfDisplay.setText(price);
                }
            }
        });

        button7.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (condition == DEPOSIT || condition == WITHDRAWAL) {
                    price += "7";
                    tfDisplay.setText(price);
                }
            }
        });

        button8.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (condition == DEPOSIT || condition == WITHDRAWAL) {
                    price += "7";
                    tfDisplay.setText(price);
                }
            }
        });

        button9.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (condition == DEPOSIT || condition == WITHDRAWAL) {
                    price += "8";
                    tfDisplay.setText(price);
                }
            }
        });

        button0.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (condition == DEPOSIT || condition == WITHDRAWAL) {
                    price += "0";
                    tfDisplay.setText(price);
                }
            }
        });

        buttonDeposit.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (condition == INTRODUCTION || condition == WITHDRAWAL) {
                    condition = DEPOSIT;
                    price = "";
                    tfDisplay.setText("Enter the required deposit amount: ");
                }
                if (condition != DEPOSIT) {
                    tfDisplay.setText("Unauthorized operation");
                }
            }
        });

        buttonWithdrawal.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if ((condition == INTRODUCTION || condition == DEPOSIT)) {
                    condition = WITHDRAWAL;
                    price = "";
                    tfDisplay.setText("Enter the amount you want to withdraw: ");
                }
                if (condition != WITHDRAWAL) {
                    tfDisplay.setText("Unauthorized operation");
                }
            }
        });

        buttonBalance.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                tfDisplay.setText("Dear " + account.getUser() + ", Your current account balance is " + String.valueOf(account.getBalance()) + " eur.");
                condition = INTRODUCTION;
            }
        });

        buttonStatement.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String text = "Transaction statement:\n";
                for (int i = 0; i < account.getTransactions().size(); i++) {
                    text += i + 1 + ". " + account.getTransactions().get(i) + "\n";
                }
                tfDisplay.setText(text);
            }
        });

        buttonUser.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                tfDisplay.setText("Account holder: " + account.getUser() + "\n\n" + account.getNumberAccount() + "\n\n" + "Telephone number: " + account.getTelephoneNumber());
            }
        });

        buttonAccount.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                tfDisplay.setText("Your account number: " + account.getNumberAccount() + "\n\n" + "Your current balance: " + account.getBalance() + "\n");
            }
        });

        buttonEnter.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (condition == DEPOSIT) {
                    account.deposit(Integer.parseInt(price));
                    tfDisplay.setText("Payment has been successfully credited to your account." + "\n\n" + "Your current account balance is: " + account.getBalance() + " eur.");
                    condition = INTRODUCTION;
                    price = "";
                } else if (condition == WITHDRAWAL) {
                    if (account.withdrawal(Integer.parseInt(price))) {
                        tfDisplay.setText("Withdrawal was successful." + "\n\n" + "Your current account balance is: " + account.getBalance() + " eur.");
                    } else {
                        tfDisplay.setText("Insufficient funds in the account.");
                    }
                    condition = INTRODUCTION;
                    price = "";
                }
            }
        });

        buttonDelete.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                price = "";
                tfDisplay.setText(price);
            }
        });

        buttonEnd.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                tfDisplay.setText("Goodbye. Have a nice day.");
            }
        });
    }

    private void initComponents() {

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 452, 449);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        JLabel TitleBankomat = new JLabel("ATM");
        TitleBankomat.setBounds(5, 5, 440, 16);
        TitleBankomat.setHorizontalAlignment(SwingConstants.CENTER);
        contentPane.add(TitleBankomat);

        buttonDeposit = new JButton("Deposit");
        buttonDeposit.setBounds(5, 144, 155, 42);
        contentPane.add(buttonDeposit);

        buttonWithdrawal = new JButton("Withdrawal");
        buttonWithdrawal.setBounds(5, 193, 155, 42);
        contentPane.add(buttonWithdrawal);

        buttonBalance = new JButton("Balance");
        buttonBalance.setBounds(5, 243, 155, 42);
        contentPane.add(buttonBalance);

        buttonStatement = new JButton("Statement");
        buttonStatement.setBounds(291, 144, 155, 42);
        contentPane.add(buttonStatement);

        buttonUser = new JButton("User");
        buttonUser.setBounds(290, 193, 155, 42);
        contentPane.add(buttonUser);

        buttonAccount = new JButton("Account");
        buttonAccount.setBounds(290, 243, 155, 42);
        contentPane.add(buttonAccount);

        buttonEnter = new JButton("Enter");
        buttonEnter.setBounds(190, 144, 80, 42);
        contentPane.add(buttonEnter);

        buttonDelete = new JButton("Delete");
        buttonDelete.setBounds(190, 193, 80, 42);
        contentPane.add(buttonDelete);

        buttonEnd = new JButton("End");
        buttonEnd.setBounds(190, 243, 80, 42);
        contentPane.add(buttonEnd);

        button1 = new JButton("1");
        button1.setBounds(138, 297, 55, 34);
        contentPane.add(button1);

        button2 = new JButton("2");
        button2.setBounds(190, 297, 55, 34);
        contentPane.add(button2);

        button3 = new JButton("3");
        button3.setBounds(243, 297, 55, 34);
        contentPane.add(button3);

        button4 = new JButton("4");
        button4.setBounds(138, 328, 55, 34);
        contentPane.add(button4);

        button5 = new JButton("5");
        button5.setBounds(190, 328, 55, 34);
        contentPane.add(button5);

        button6 = new JButton("6");
        button6.setBounds(243, 328, 55, 34);
        contentPane.add(button6);

        button7 = new JButton("7");
        button7.setBounds(138, 358, 55, 34);
        contentPane.add(button7);

        button8 = new JButton("8");
        button8.setBounds(190, 358, 55, 34);
        contentPane.add(button8);

        button9 = new JButton("9");
        button9.setBounds(243, 358, 55, 34);
        contentPane.add(button9);

        button0 = new JButton("0");
        button0.setBounds(190, 387, 55, 34);
        contentPane.add(button0);

        tfDisplay = new JTextArea();
        tfDisplay.setBounds(5, 33, 440, 99);
        contentPane.add(tfDisplay);
        tfDisplay.setColumns(10);
        tfDisplay.setRows(4);
        tfDisplay.setText("Welcome to our demo ATM.");

    }
}
