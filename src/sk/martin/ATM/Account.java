package sk.martin.ATM;

import java.util.ArrayList;

public class Account {

    private int balance;
    private String user;
    private String numberAccount;
    private final String telephoneNumber;

    private final ArrayList<String> transactions = new ArrayList<>();

    public Account(String user, String numberAccount, int balance, String telephoneNumber) {
        this.user = user;
        this.numberAccount = numberAccount;
        this.balance = balance;
        this.telephoneNumber = telephoneNumber;
    }

    public String getTelephoneNumber() {
        return telephoneNumber;
    }

    public ArrayList<String> getTransactions() {
        return transactions;
    }

    public void deposit(int enteredPrice) {
        if (enteredPrice > 0) {
            balance += enteredPrice;
        }
        transactions.add("You have just selected: " + enteredPrice + " eur.");
    }

    public boolean withdrawal(int enteredPrice) {
        if (enteredPrice <= balance) {
            balance -= enteredPrice;
            transactions.add("You have just selected: " + enteredPrice + " eur.");
            return true;
        } else  {
            return false;
        }
    }

    public String getNumberAccount() {
        return numberAccount;
    }

    public void setNumberAccount(String numberAccount) {
        this.numberAccount = numberAccount;
    }

    public int getBalance() {
        return balance;
    }


    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

}
